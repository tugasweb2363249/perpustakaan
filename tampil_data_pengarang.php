<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tampil Data Pengarang</title>
</head>
<body>
	<?php  
		include'config.php';
		$db = new Database();
	?>
	<table border="1">
	<tr>
		<th>No</th>
		<th>Kode Pengarang</th>
		<th>Nama Pengarang</th>
		<th>Edit</th>
		<th>Hapus</th>
	</tr>
	<?php  
	$no = 1;
	foreach($db->tampil_data_pengarang() as $x){
	?>
	<tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo $x['kode_pengarang']; ?></td>
		<td><?php echo $x['nama_pengarang']; ?></td>
		<td><a href="edit_data_pengarang.php?id=<?php echo $x['kode_pengarang']; ?>">Edit</a></td>
		<td><a href="hapus_data_pengarang.php?id=<?php echo $x['kode_pengarang']; ?>">Hapus</a></td>
	</tr>
	<?php  
	}
	?>
	</table>
	<div>
		<a href="tambah_data_pengarang.php">Tambah Data Pengarang</a>
		<a href="index.php">Home</a>
	</div>
</body>
</html>